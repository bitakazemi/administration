Please add your UGA ID, your bitbucket ID, and the paper you wish to present by any available date. Don't worry if the table columns don't line up in the Markdown code.

Here's an example:

Date   | UGA ID | BitBucket ID | Paper title (and link) |
------ | ------ | ------------ | ---------------------- |
1/6    | spq    | magsol       | [CSCI 6900 Lecture 1](http://cobweb.cs.uga.edu/~squinn/mmd_s15/lectures.html)  |

Please sign up here **by Friday, Jan 9.** Anyone not signed up by then will be randomly assigned a presentation date and paper. See the website for a list of paper suggestions. If you choose not to use one of the papers listed, please seek my approval first (I'll most likely say yes).

Date   | UGA ID | BitBucket ID | Paper title (and link) |
------ | ------ | ------------ | ---------------------- |
Jan 13 |        |              |                        |
Jan 15 |        |              |                        |
Jan 20 |mchurc90|michaelchurch90|[Style in the Long Tail: Discovering Unique Interests with Latent Variable Models in Large Scale Social E-commerce ](http://cobweb.cs.uga.edu/~squinn/mmd_s15/papers/p1640-hu.pdf)|
Jan 22 |navidht	|navidht	   |[Knowledge Vault: A Web-Scale Approach to Probabilistic Knowledge Fusion](http://www.cs.cmu.edu/~nlao/publication/2014.kdd.pdf)                       |
Jan 27 |        |              |                        |
Feb 3  |bitak   |bitakazemi    |[Distributed Approximate Spectral Clustering for Large-Scale Datasets](http://cobweb.cs.uga.edu/~squinn/mmd_s15/papers/p223-hefeeda.pdf)                       |
Feb 5  |wdr525  |wdr525        |[Distributed PCA and k-Means Clustering](http://cobweb.cs.uga.edu/~squinn/mmd_s15/papers/DistributedPCA.pdf)|
Feb 10 |anthozoa|anthozoa      |[Influenza-Like Illness Surveillance on Twitter through Automated Learning of Naïve Language](http://www.plosone.org/article/fetchObject.action?uri=info%3Adoi%2F10.1371%2Fjournal.pone.0082489&representation=PDF)|                        |
Feb 12 |ac59478 |Alekhya_Ch    |[Distributed GraphLab: A Framework for Machine Learning and Data Mining in the Cloud](http://cobweb.cs.uga.edu/~squinn/mmd_s15/papers/1204.6078.pdf)|
Feb 17 |mr40613 | manishatuga  |[Minerva: A scalable and highly efficient tracking platform for deep learning](http://cobweb.cs.uga.edu/~squinn/mmd_s15/papers/Minerva.pdf)                     |
Feb 24 |ksamim  |ksamim        |[Tracking Climate Change Opinions from Twitter Data](http://cobweb.cs.uga.edu/~squinn/mmd_s15/papers/KDD_Twitter_ClimateChange.pdf)|
Mar 3  |hp240955|hp240955      |[Fast, Accurate Detection of 100,000 Object Classes on a Single Machine](http://cobweb.cs.uga.edu/~squinn/mmd_s15/papers/2013%20Dean.pdf)
Mar 19 |bma09868|bahaelaila7   |[Discretized Streams: Fault-Tolerant Streaming Computation at Scale](http://cobweb.cs.uga.edu/~squinn/mmd_s15/papers/sosp_spark_streaming.pdf)|
Mar 24 |yinyue  |yinyueacm     |[Large-Scale High-Precision Topic Modeling on Twitter](http://cobweb.cs.uga.edu/~squinn/mmd_s15/papers/p1907-yang.pdf)|
Mar 26 |mkran   |mkran1985     |[Event Detection via Communication Pattern Analysis](http://cobweb.cs.uga.edu/~squinn/mmd_s15/papers/8088-37732-1-PB.pdf)|
Mar 31 |apj32468|  ankitajo    |[NIFTY: A System for Large Scale Information Flow Tracking and Clustering](http://cobweb.cs.uga.edu/~squinn/mmd_s15/papers/nifty-www13.pdf)|                        |
Apr 7  |yangbai |yang_bai      |[Using Friends as Sensors to Detect Global-Scale Contagious Outbreaks](http://cobweb.cs.uga.edu/~squinn/mmd_s15/papers/journal.pone.0092413.pdf)                     |
Apr 9  |liu123  |zhaochongliu  |[An Algorithm for the Principal Component Analysis of Large Data Sets](http://cobweb.cs.uga.edu/~squinn/mmd_s15/papers/arXiv%202010%20Halko.pdf)|
